package com.ee.currencypedia;

import com.rabbitmq.client.impl.Environment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
class StatisticCollectorServiceImplTest {

    @Mock
    private CurrencyInfoRepository currencyInfoRepository;

    @InjectMocks
    private StatisticCollectorServiceImpl statisticCollectorService;

    public void setUp() {
        statisticCollectorService = new StatisticCollectorServiceImpl(currencyInfoRepository);
    }

    @Test
    void returnCurrentCurrencyInfoIfDataIsAvailable() {

        String currency = "EUR";

        CurrencyInfo currencyInfo = new CurrencyInfo();
        currencyInfo.setId(15L);

        statisticCollectorService.getCurrentCurrencyValues(currency);

        verify(currencyInfoRepository, times(1)).findTopByBaseOrderByTimestampDesc(currency);
    }


    @Test
    public void whenGettingCurrentStatisticsThrowExceptionWhenGivenCurrencyIsNotSupported() {

        String currency = "SAD";

        assertThrows(UnsupportedCurrencyException.class, () -> {
            statisticCollectorService.getCurrentCurrencyValues(currency);
        });

        verify(currencyInfoRepository, never()).findTopByBaseOrderByTimestampDesc(currency);
    }

    @Test
    void returnHistoryCurrencyInfoIfDataIsAvailable() {

        String currency = "EUR";
        int hours = 24;

        CurrencyInfo currencyInfo = new CurrencyInfo();
        currencyInfo.setId(15L);

        statisticCollectorService.getHistory(currency, hours);

        //TODO: we assume here that the correct parameters are given but we need to think how to check the timestamp. A way would be to have our own converter class and mock it so we can use the value
        verify(currencyInfoRepository, times(1)).findCurrencyInfoByBaseAndTimestampGreaterThanOrderByTimestampDesc(anyString(), anyLong());
    }


    @Test
    void whenGettingHistoryStatisticsThrowExceptionWhenGivenCurrencyIsNotSUpported() {

        String currency = "SAD";
        int hours = 24;

        assertThrows(UnsupportedCurrencyException.class, () -> {
            statisticCollectorService.getHistory(currency, hours);
        });

        verify(currencyInfoRepository, never()).findCurrencyInfoByBaseAndTimestampGreaterThanOrderByTimestampDesc(anyString(), anyLong());
    }
}