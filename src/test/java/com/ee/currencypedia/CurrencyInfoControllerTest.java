package com.ee.currencypedia;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = CurrencypediaApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CurrencyInfoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatisticCollectorService statisticCollectorService;

    @Test
    public void currencyInfoIsReturned() throws Exception {
        CurrencyInfoRequest currencyInfoRequest = new CurrencyInfoRequest();
        currencyInfoRequest.setCurrency("EUR");
        ObjectMapper mapper = new ObjectMapper();
        String currencyInfoRequestAsJson = mapper.writeValueAsString(currencyInfoRequest);
        mockMvc.perform(post("/json_api/current").contentType(MediaType.APPLICATION_JSON).content(currencyInfoRequestAsJson)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void userWithEmailThatIsAlreadyExistingReturnsConflict() throws Exception {
        CurrencyInfoRequest currencyInfoRequest = new CurrencyInfoRequest();
        currencyInfoRequest.setCurrency("SAD");
        ObjectMapper mapper = new ObjectMapper();
        String currencyInfoRequestAsJson = mapper.writeValueAsString(currencyInfoRequest);
        doThrow(new UnsupportedCurrencyException("SAD")).when(statisticCollectorService).getCurrentCurrencyValues("SAD");
        mockMvc.perform(post("/json_api/current").contentType(MediaType.APPLICATION_JSON).content(currencyInfoRequestAsJson)).andDo(print()).andExpect(status().isBadRequest());
    }

    //SAme test must be written and for the other method plus more test for other cases with the hours
}