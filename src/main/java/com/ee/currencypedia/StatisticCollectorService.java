package com.ee.currencypedia;

import java.util.Date;
import java.util.List;

public interface StatisticCollectorService {

    /**
     * Used to get the most current information for a supported currency
     * @param baseCurrency the currency for which we want information
     * @return information with the rates of the currency via other currencies
     */
    CurrencyInfo getCurrentCurrencyValues(String baseCurrency);

    /**
     * Used to get all available statistics for a currency from the current time back to some time
     * @param baseCurrency the currency for which we want information
     * @param hours how many hours back we want information i.e 24 would mean from now to 24 hours back
     * @return information with all rates of the currency via other currencies for the perio
     */
    List<CurrencyInfo> getHistory(String baseCurrency, Integer hours);
}
