package com.ee.currencypedia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "json_api")
public class CurrencyInfoController {

    private final StatisticCollectorService statisticCollectorService;

    @Autowired
    public CurrencyInfoController(StatisticCollectorService statisticCollectorService) {
        this.statisticCollectorService = statisticCollectorService;
    }

    @PostMapping(value = "current")
    public CurrencyInfo getCurrentRates(@RequestBody CurrencyInfoRequest request) {

        return statisticCollectorService.getCurrentCurrencyValues(request.getCurrency());

    }

    @PostMapping(value = "history")
    public List<CurrencyInfo> getRatesForPeriod(@RequestBody CurrencyInfoRequest request) {

        return statisticCollectorService.getHistory(request.getCurrency(), request.getPeriodInHours());
    }
}
