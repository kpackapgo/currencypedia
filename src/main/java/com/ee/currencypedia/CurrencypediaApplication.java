package com.ee.currencypedia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CurrencypediaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencypediaApplication.class, args);
	}

}
