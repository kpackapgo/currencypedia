package com.ee.currencypedia;

import javax.persistence.*;
import java.util.Map;

@Entity
public class CurrencyInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private boolean success;

    private Long timestamp;

    private String base;


    @ElementCollection
    @CollectionTable(name = "order_item_mapping", joinColumns = {@JoinColumn(name = "currency_info_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "currency")
    @Column(name = "value")
    private Map<String, Double> rates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }
}
