package com.ee.currencypedia;

public class CurrencyInfoRequest {

    private Long clientId;

    private Long timestamp;

    private String currency;

    private Integer periodInHours;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getPeriodInHours() {
        return periodInHours;
    }

    public void setPeriodInHours(Integer periodInHours) {
        this.periodInHours = periodInHours;
    }
}
