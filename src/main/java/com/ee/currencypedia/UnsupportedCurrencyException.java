package com.ee.currencypedia;

public class UnsupportedCurrencyException extends RuntimeException {

    public UnsupportedCurrencyException(String unsupportedCurrency) {
        super(unsupportedCurrency +" is NOT supported!");
    }
}
