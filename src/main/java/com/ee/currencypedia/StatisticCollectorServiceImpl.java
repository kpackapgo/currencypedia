package com.ee.currencypedia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class StatisticCollectorServiceImpl implements StatisticCollectorService {

    @Value("${supported_base_currencies}")
    private String supportedBaseCurrenciesAsString;

    private final CurrencyInfoRepository currencyInfoRepository;

    @Autowired
    public StatisticCollectorServiceImpl(CurrencyInfoRepository currencyInfoRepository) {

        this.currencyInfoRepository = currencyInfoRepository;
    }

    @Override
    public CurrencyInfo getCurrentCurrencyValues(String baseCurrency) {
        if (!isCurrencySupported(baseCurrency)) {
            throw new UnsupportedCurrencyException(baseCurrency);
        }
        return currencyInfoRepository.findTopByBaseOrderByTimestampDesc(baseCurrency);
    }

    @Override
    public List<CurrencyInfo> getHistory(String baseCurrency, Integer hours) {
        if (!isCurrencySupported(baseCurrency)) {
            throw new UnsupportedCurrencyException(baseCurrency);
        }
        //TODO: validation of hours parameter to be positive number and also handle timezones correctly
        Date currentDate = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(hours));
        return currencyInfoRepository.findCurrencyInfoByBaseAndTimestampGreaterThanOrderByTimestampDesc(baseCurrency, currentDate.getTime());
    }

    private boolean isCurrencySupported(String currency) {
        List<String> supportedBaseCurrencies = Arrays.asList(supportedBaseCurrenciesAsString.split(",", -1));

        return supportedBaseCurrencies.contains(currency);
    }
}
