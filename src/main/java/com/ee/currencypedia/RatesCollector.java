package com.ee.currencypedia;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class RatesCollector {

    @Value("${api_url}")
    private String apiUrl;

    @Value("${api_key}")
    private String apiKey;

    @Value("${supported_base_currencies}")
    private String supportedBaseCurrenciesAsString;

    /**
     * Automatically executed method by the scheduler which is used to get the latest rates
     * for each currency defined in the application.properties
     */
    @Scheduled(fixedRateString = "${api_fetch_interval_in_miliseconds}", initialDelay = 5000)
    private void scheduleFixedRateTaskAsync() {
        StringBuilder urlBuilder = new StringBuilder(apiUrl);
        urlBuilder.append("?");
        urlBuilder.append("access_key=");
        urlBuilder.append(apiKey);
        urlBuilder.append("&");
        urlBuilder.append("base=");

        List<String> supportedBaseCurrencies = Arrays.asList(supportedBaseCurrenciesAsString.split(",", -1));

        RestTemplate restTemplate = new RestTemplate();

        for (String supportedBaseCurrency : supportedBaseCurrencies) {
            String urlForCurrency = urlBuilder.toString()+supportedBaseCurrency;
            System.out.println(urlForCurrency);
            CurrencyInfo result = restTemplate.getForObject(urlForCurrency, CurrencyInfo.class);

            //TODO: Store all extracted rates to the database
        }
    }
}
