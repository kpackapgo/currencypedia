package com.ee.currencypedia;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CurrencyInfoRepository extends CrudRepository<CurrencyInfo, Long> {

    CurrencyInfo findTopByBaseOrderByTimestampDesc(String baseCurrency);

    List<CurrencyInfo> findCurrencyInfoByBaseAndTimestampGreaterThanOrderByTimestampDesc(String baseCurrency, Long timestamp);
}
